import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_home/models/device_model.dart';

class DeviceOverviewWidget extends StatelessWidget {
  final DeviceModel device;

  const DeviceOverviewWidget({Key? key, required this.device})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.blue.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(50),
                ),
                padding: const EdgeInsets.all(10),
                child:
                    Icon(device.deviceIcon, color: Colors.blueAccent, size: 30),
              ),
              Switch(
                  value: device.isDeviceOn,
                  onChanged: (val) {},
                  splashRadius: 5),
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 5, bottom: 10),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(device.deviceName,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 13)),
              Container(
                margin: const EdgeInsets.only(top: 5),
              ),
              Text('Consuming ' + device.deviceConsume.toString() + ' kWh',
                  style: const TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 12,
                      color: Colors.grey))
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 15),
          ),
          Text(device.deviceStream,
              style: const TextStyle(
                  color: Colors.pinkAccent, fontWeight: FontWeight.w600))
        ],
      ),
    );
  }
}
