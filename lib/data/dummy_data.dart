import 'package:flutter/material.dart';
import 'package:smart_home/models/device_model.dart';

class DummyData {
  static List<DeviceModel> devices = [
    DeviceModel(Icons.air, 'Air Conditioner', 8, '2h 45min', true),
    DeviceModel(Icons.light_outlined, 'Smart Lamp', 0.7, '5h 37min', false),
    DeviceModel(Icons.tv, 'Smart TV', 2, '3h 22min', false),
    DeviceModel(Icons.speaker, 'Speaker', 0.9, '1h 02min', true),
    DeviceModel(Icons.kitchen, 'Refrigerator', 25, '89h 59min', true),
    DeviceModel(Icons.shower_outlined, 'Heat Pump', 0.8, '0h 16min', true),
  ];
}
