import 'package:flutter/cupertino.dart';

class DeviceModel {
  final IconData deviceIcon;
  final String deviceName;
  final double deviceConsume;
  final String deviceStream;
  final bool isDeviceOn;

  DeviceModel(this.deviceIcon, this.deviceName, this.deviceConsume, this.deviceStream, this.isDeviceOn);
}
