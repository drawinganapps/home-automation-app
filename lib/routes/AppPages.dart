import 'package:get/get.dart';
import 'package:smart_home/routes/AppRoutes.dart';
import 'package:smart_home/screens/dashboard_screen.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.DASHBOARD,
        page: () => const DashboardScreen(),
        transition: Transition.rightToLeft)
  ];
}
