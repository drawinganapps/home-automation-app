import 'package:flutter/material.dart';

ThemeData lightTheme = ThemeData(
  brightness: Brightness.light,
  scaffoldBackgroundColor: Colors.white.withOpacity(0.95),
  highlightColor: Colors.transparent,
  splashColor: Colors.transparent,
  fontFamily: 'San Francisco'
);
