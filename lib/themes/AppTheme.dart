import 'package:flutter/material.dart';
import 'package:smart_home/themes/DarkTheme.dart';
import 'package:smart_home/themes/LigthTheme.dart';

class AppTheme {
  static ThemeData light = lightTheme;
  static ThemeData dark = darkTheme;
}
