import 'package:badges/badges.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:smart_home/data/dummy_data.dart';
import 'package:smart_home/models/device_model.dart';
import 'package:smart_home/widgets/device_overview_widget.dart';

class DashboardScreen extends StatelessWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<DeviceModel> devices = DummyData.devices;
    return Scaffold(
        body: Column(
          children: [
            Container(
              padding:
              const EdgeInsets.only(top: 50, left: 15, right: 15, bottom: 30),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomLeft,
                    colors: [
                      Colors.orangeAccent.shade100,
                      Colors.white24,
                    ],
                  )),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text('Welcome, Mirlan!',
                          style: TextStyle(
                              fontWeight: FontWeight.w700, fontSize: 18)),
                      Container(
                        margin: const EdgeInsets.only(top: 5),
                      ),
                      const Text('Always save on using Electricity',
                          style: TextStyle(
                              color: Colors.grey, fontWeight: FontWeight.w500)),
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        color: Colors.orange,
                        borderRadius: BorderRadius.circular(50)),
                    child: Image.asset(
                      'assets/img/beard.png',
                      width: 30,
                      height: 30,
                    ),
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Container(
                padding: const EdgeInsets.only(
                    top: 15, left: 15, right: 15, bottom: 30),
                decoration: BoxDecoration(
                    color: Colors.white, borderRadius: BorderRadius.circular(15)),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Energy Usage',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black.withOpacity(0.8))),
                        Row(
                          children: [
                            const Icon(Icons.today,
                                color: Colors.blueAccent, size: 15),
                            Container(
                              margin: const EdgeInsets.only(left: 5),
                              child: const Text('9 April 2021',
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold)),
                            ),
                            const Icon(
                              Icons.expand_more,
                              size: 16,
                              color: Colors.grey,
                            )
                          ],
                        ),
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 30),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            const Icon(Icons.electrical_services,
                                color: Colors.green),
                            Container(
                              margin: const EdgeInsets.only(right: 10),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                Text('Today',
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontWeight: FontWeight.bold)),
                                Text('28.6 kWh',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16)),
                              ],
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            const Icon(Icons.bolt, color: Colors.pinkAccent),
                            Container(
                              margin: const EdgeInsets.only(right: 10),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: const [
                                Text('This Month',
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontWeight: FontWeight.bold)),
                                Text('325.37 kWh',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16)),
                              ],
                            ),
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(top: 30, left: 15, right: 15),
              child: Container(
                padding: const EdgeInsets.only(
                    top: 15, left: 20, right: 20, bottom: 15),
                decoration: BoxDecoration(
                    color: Colors.blueAccent,
                    borderRadius: BorderRadius.circular(15)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Flexible(
                        child: Text(
                          'Your TV is not used, turn it of to save electricity.',
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w600),
                        )),
                    Container(
                        margin: const EdgeInsets.only(left: 30),
                        child: ElevatedButton(
                          onPressed: () {},
                          style: ButtonStyle(
                            shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(18.0),
                                )),
                            backgroundColor: MaterialStateProperty.all<Color>(
                                Colors.orangeAccent),
                          ),
                          child: const Text('Turn Off'),
                        )),
                  ],
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.only(top: 30, left: 15, right: 15, bottom: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('My Device',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  Row(
                    children: [
                      const Text(
                        'View all',
                        style: TextStyle(
                            color: Colors.orangeAccent,
                            fontWeight: FontWeight.bold),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 5),
                        decoration: BoxDecoration(
                            color: Colors.orangeAccent,
                            borderRadius: BorderRadius.circular(2)),
                        child: const Icon(
                          Icons.expand_more,
                          color: Colors.white,
                          size: 15,
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            Expanded(
              child: GridView.count(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  crossAxisCount: 2,
                  crossAxisSpacing: 20,
                  mainAxisSpacing: 20,
                  children: List.generate(devices.length, (index) {
                    return DeviceOverviewWidget(device: devices[index],);
                  })),
            )
          ],
        ),
        bottomNavigationBar: Container(
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30),
            ),
          ),
          height: 80,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                enableFeedback: false,
                onPressed: () {},
                icon: const Icon(
                  Icons.space_dashboard,
                  color: Colors.orange,
                  size: 30,
                ),
              ),
              IconButton(
                enableFeedback: false,
                onPressed: () {},
                icon: const Icon(
                  Icons.insert_chart_outlined,
                  color: Colors.grey,
                  size: 30,
                ),
              ),
              Container(
                height: 55,
                width: 55,
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(50)
                ),
                child: IconButton(
                  enableFeedback: false,
                  onPressed: () {},
                  icon: const Icon(
                    Icons.add_circle_outline,
                    color: Colors.white,
                    size: 30,
                  ),
                ),
              ),
              IconButton(
                enableFeedback: false,
                onPressed: () {},
                icon: Badge(
                  position: BadgePosition.topEnd(top: 3, end: 3),
                  badgeColor: Colors.pinkAccent,
                  child: const Icon(
                    Icons.notifications_none,
                    color: Colors.grey,
                    size: 30,
                  ),
                ),
              ),
              IconButton(
                enableFeedback: false,
                onPressed: () {},
                icon: const Icon(
                  Icons.settings_outlined,
                  color: Colors.grey,
                  size: 30,
                ),
              ),
            ],
          ),
        )
    );
  }
}
